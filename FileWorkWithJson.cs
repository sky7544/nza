﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text.Json.Serialization;
using System.Threading.Channels;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;


namespace FileWorkWithJson
{

    
    public class FileWorkWithJson
    {
       
        #region Interface

        public static void SJsonDynamicDict(string inputPath, string fileName, Dictionary<string, dynamic> input)
        {
            string path = $@"{inputPath}\{fileName}";

            //Сериализуем в строку
            string jsonString = JsonConvert.SerializeObject(input);

            //Чекаем и бэкапаем файл
            CheckTheFileExistsAndBackup(inputPath, fileName);

            //Перезаписываем файл
            StreamWriter stWriter = new StreamWriter(path, false, System.Text.Encoding.Default);  
            stWriter.Write(jsonString);
            stWriter.Close();

            Console.WriteLine($"Настройки сохранены по адресу: {path}");
        }//Для сохранения основных настроек. Если нужно поменять сохраненные настройки, то сначала получить их, изменить и сохранить их словарем\ми используя этот метод

        public static void SJsonDynamicDict(string inputPath, string fileName, string key, dynamic input)
        {
            string path = $@"{inputPath}\{fileName}";
            
            //Загружаем файл
            Dictionary<string, dynamic> loadedDictionary = DJsonReturnDynamicDictionaryJConvert(inputPath, fileName); 
            
            //Добавляем переданные данные в конец и сериализуем в строку
            loadedDictionary.TryAdd(key, input);
            string jsonString = JsonConvert.SerializeObject(loadedDictionary);

            //Чекаем и бэкапаем файл
            CheckTheFileExistsAndBackup(inputPath, fileName);

            //Перезаписываем файл
            StreamWriter stWriter = new StreamWriter(path, false, System.Text.Encoding.Default);
            stWriter.Write(jsonString);
            stWriter.Close();

            Console.WriteLine($"Файл дополнен и сохранен по адресу: {path}"); 
        }//Для сохранения чего угодно в ручную в существующий файл. Добавляет в конце и не перезаписывает существующие элементы

        public static dynamic DJsonReturnDynamicDictionaryJConvert(string inputPath, string fileName) //Загружает из файла. Требует подключения Newtonsoft.Json
        {
            string path = $@"{inputPath}\{fileName}";
            FileInfo fileInfo = new FileInfo(path);
            FileInfo BfileInfo = new FileInfo($@"{path}_buckup.json");

            if ((!fileInfo.Exists || fileInfo.Length == 0) && (!BfileInfo.Exists || BfileInfo.Length == 0))
            {
                CreateTheFileJson(inputPath, fileName);
            }
            else
            {
                File.Delete($@"{path}");
                File.Copy($@"{path}_buckup.json", $@"{path}");
                Console.WriteLine("Файл восстановлен из бэкапа! Нужно проверить актуальность данных");
            }

            dynamic result = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(File.ReadAllText($@"{path}"));
            return result;
        }

        #endregion



        #region AuxiliaryLogic

        private static void CreateTheFileJson(string inputPath, string fileName)
        {
            string path = $@"{inputPath}\{fileName}";

            Console.WriteLine("Файл с настройками не найден или пуст! Нужно указать значения по умолчанию.");

            Console.WriteLine("Введите курс доллара (цифры, запятая): ");
            decimal rub = Convert.ToDecimal(Console.ReadLine());
            decimal usd = 1m;
            Console.WriteLine();

            Console.WriteLine("Введите начальный вес для сети (цифры, запятая): ");
            decimal weight = Convert.ToDecimal(Console.ReadLine());

            Dictionary<string, dynamic> newFile = new Dictionary<string, dynamic>();
            newFile.TryAdd("Parameters1", new Dictionary<string, object>{ ["usd"] = usd });
            newFile["Parameters1"]["rub"] = rub;
            newFile["Parameters1"]["weight"] = weight;

            string jsonString = JsonConvert.SerializeObject(newFile);
            StreamWriter stWriter = new StreamWriter(path, false, System.Text.Encoding.Default);
            stWriter.Write(jsonString);
            stWriter.Close();
        }

        private static void CheckTheFileExistsAndBackup(string inputPath, string fileName)
        {
            string path = $@"{inputPath}\{fileName}";
            FileInfo fileInfo = new FileInfo(path);
            FileInfo BfileInfo = new FileInfo($@"{path}_buckup.json");

            if (!fileInfo.Exists || fileInfo.Length == 0)
            {
                if (!BfileInfo.Exists || BfileInfo.Length == 0)
                {
                    CreateTheFileJson(inputPath, fileName);
                }
                else
                {
                    File.Delete($@"{path}");
                    File.Copy($@"{path}_buckup.json", $@"{path}");
                    Console.WriteLine("Файл восстановлен из бэкапа! Нужно проверить актуальность данных");
                }
            }
            else
            {
                File.Delete($@"{path}_buckup.json");
                File.Copy($@"{path}", $@"{path}_buckup.json");
                Console.WriteLine($"Создан бэкап файла по пути: {path}_buckup.json");
            }
        }

        #endregion

    }
}