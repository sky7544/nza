﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
	public class Program
    {
        private static void TrainNeuron1(NetWork.NeuronSloi1 neuron, decimal input1, decimal input2)
        {
			int i = 0;
			do
			{
				i++;

				neuron.Train(input1, input2);

				if (i % 10000000 == 0)
				{
					Console.WriteLine($"Итерация: {i}\tОшибка:\t{NetWork.LastError}");
				}
			} while (NetWork.LastError > neuron.Smoothing || NetWork.LastError < -neuron.Smoothing);
		}

		private static void TrainNeuron2(NetWork.NeuronSloi2 neuron, decimal input1, decimal input2)
		{
			int i = 0;
			do
			{
				i++;

				neuron.Train(input1, input2);

				if (i % 10000000 == 0)
				{
					Console.WriteLine($"Итерация: {i}\tОшибка:\t{NetWork.LastError}");
				}
			} while (NetWork.LastError > neuron.Smoothing || NetWork.LastError < -neuron.Smoothing);
		}

        private static void SaveWeightOnFile(string inputPath ,string fileName, string key, decimal input)
        {
            string path = $@"{inputPath}";
			DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists){ dirInfo.Create(); }

            path = $@"{inputPath}\{fileName}";
			FileInfo fileInfo = new FileInfo(path);
			if (!fileInfo.Exists){ fileInfo.Create(); }

            Dictionary<string, decimal> dictionaryForSerialize = new Dictionary<string, decimal> { [key] = input };
			StreamWriter stWriter = new StreamWriter(path, false, System.Text.Encoding.Default);
            stWriter.WriteLine(dictionaryForSerialize);
            stWriter.Close();
		}

		static void Main(string[] args)
		{
            //Загрузка
            //string path = @"C:\Users\nshakirov\Documents\Visual Studio 2019\repo\ololoNetWork2Neuron\ConsoleApp1";
			string path = @"C:\Users\Никита\Documents\nza\ololoNetWork2Neuron\ConsoleApp1";// Нужно вписать свой путь к файлу Джейсон. Передается в процедуры
			string fileName = "Json.json";//Вписать имя файла. Передается в процедуры
            var deserializedDictionary = FileWorkWithJson.DJsonReturnDynamicDictionaryJConvert(path, fileName);//Загружает настройки



            decimal usd = deserializedDictionary["Parameters1"]["usd"];
            decimal rub = deserializedDictionary["Parameters1"]["rub"];
            NetWork.weight = deserializedDictionary["Parameters1"]["weight"];

			NetWork.NeuronSloi1 neuron1 = new NetWork.NeuronSloi1();
			NetWork.NeuronSloi2 neuron2 = new NetWork.NeuronSloi2();

            TrainNeuron1(neuron1, usd, rub);
            Console.WriteLine("Обучение neuron1 завершено!");

            TrainNeuron2(neuron2, usd, rub);
            Console.WriteLine("Обучение завершено!");
            
			Console.WriteLine("Сколько баксонов будем конвертить: ");
			usd = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine($"{neuron2.ProcessInputData(usd)} rub в {usd} usd");
            


            //Сохранение
            deserializedDictionary["Parameters1"]["weight"] = NetWork.weight;//Сохраняет вес в настройках
            FileWorkWithJson.SJsonDynamicDict(path, fileName, deserializedDictionary);//Сохраняет настройки
		}
	}
}

