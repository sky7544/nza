﻿using System.Runtime.InteropServices.ComTypes;

namespace ConsoleApp1
{
	class NetWork
    {
        public static decimal weight { get; set; }

		public static decimal LastError { get; private set; }


		public class NeuronSloi1
		{
			public decimal Smoothing { get; private set; } = 0.00001m;

			public void Train(decimal input, decimal expectedResult)
			{
				var actualResult = input * weight;
				LastError = expectedResult - actualResult;
				var correction = (LastError / actualResult) * Smoothing;
				weight += correction;
			}
		}

		public class NeuronSloi2
		{
			public decimal Smoothing { get; private set; } = 0.000001m;

			public decimal ProcessInputData(decimal input)
			{
				return input * weight;
			}

			public decimal RestoreInputData(decimal output)
			{
				return output / weight;
			}

			public void Train(decimal input, decimal expectedResult)
			{
				var actualResult = input * weight;
				LastError = expectedResult - actualResult;
				var correction = (LastError / actualResult) * Smoothing;
				weight += correction;
			}
		}
	}
}
